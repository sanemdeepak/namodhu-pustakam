/**
 * Created by sanemdeepak on 12/17/16.
 */
import React from "react";

export default class Layout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mockLayout: ''
        };

        this.JSONToDOMBuilder = this.JSONToDOMBuilder.bind(this);
    }

    componentDidMount() {


        let userData = "{\n" +
            "\t\"user\":[\n" +
            "\t\t{\n" +
            "\t\t\t\"type\":\"name\",\n" +
            "\t\t\t\"value\":\"deepak\",\n" +
            "\t\t\t\"order\":\"1\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"type\":\"age\",\n" +
            "\t\t\t\"value\":\"24\",\n" +
            "\t\t\t\"order\":\"2\"\n" +
            "\t\t}]\n" +
            "}";


        let jsonData = "{\n" +
            "  \"components\":[\n" +
            "   {\n" +
            "    \"type\": \"text\",\n" +
            "    \"id\":\"name\",\n" +
            "    \"class\":\"txtarea-1\",\n" +
            "    \"text\":\"123\",\n" +
            "    \"placeholder\":\"enter data\"\n" +
            "    },\n" +
            "   {\n" +
            "    \"type\": \"text\",\n" +
            "    \"id\":\"age\",\n" +
            "    \"class\":\"txtarea-2\",\n" +
            "    \"text\":\"123\",\n" +
            "    \"placeholder\":\"enter data\"\n" +
            "    }]\n" +
            "    \n" +
            "}";
        let mock = this.JSONToDOMBuilder(jsonData, userData);
        this.setState({mockLayout: mock});
    }


    render() {
        return (
            <div>
                {this.state.mockLayout};
            </div>
        );
    }

    //parse json and build required components
    JSONToDOMBuilder(val, userData) {
        let dom = [];
        let uiObject = JSON.parse(val);
        let boObject = JSON.parse(userData);
        for (let x = 0; x < boObject.user.length; x++) {
            dom.push(<div>
                <input
                    type={uiObject.components[x].type}
                    id={uiObject.components[x].id}
                    class={uiObject.components[x].class}
                    value={boObject.user[x].value}>
                </input>
            </div>);
        }
        return dom;
    }
}