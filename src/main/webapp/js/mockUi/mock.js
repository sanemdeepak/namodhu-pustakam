/**
 * Created by sanemdeepak on 12/17/16.
 */

import React from "react";
import ReactDOM from "react-dom";

import Layout from "./components/Layout";

const app = document.getElementById('mock');
ReactDOM.render(<Layout/>, app);